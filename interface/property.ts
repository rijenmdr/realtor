export interface PropertyType {
    id: number;
    externalID: number;
    externalId: string;
    photos: {
      id: string;
      url: string;
    }[];
    coverPhoto: {
      url: string;
    };
    description: string;
    isVerified: boolean;
    price: number;
    rentFrequency: string;
    agency: {
      logo: {
        url: string;
      };
    };
    rooms: number;
    baths: number;
    area: number;
    title: string;
    type:string;
    purpose:string;
    furnishingStatus: string;
    amenities: amenitiesType[]
}

interface amenitiesType {
  amenities: {
    text: string
  }[];
}