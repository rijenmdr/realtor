export interface locationType {
    externalID: number;
    id: number;
    name: string;
}