export interface queryType {
  purpose: string;
  rentFrequency: string;
  minPrice: string;
  maxPrice: string;
  roomsMin: string;
  bathsMin: string;
  sort: string;
  areaMax: string;
  locationExternalIDs: string;
  categoryExternalID: string;
}
