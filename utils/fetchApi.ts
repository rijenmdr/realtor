import axios from "axios";

export const baseUrl = "https://bayut.p.rapidapi.com/";

export const fetchApi = async (url: string) => {
  const { data } = await axios.get(url, {
    headers: {
      "X-RapidAPI-Host": "bayut.p.rapidapi.com",
      "X-RapidAPI-Key": "902a57655fmsh8eefb6a3f602580p19ea3cjsnf5e1ee01037a",
    },
  });

  return data;
};
