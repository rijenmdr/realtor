import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { BsFilter } from "react-icons/bs";
import Property from "../components/Property/Property";
import SearchFilter from "../components/SearchFilter/SearchFilter";
import { PropertyType } from "../interface/property";
import { queryType } from "../interface/query";

import Empty from '../public/images/empty.svg';
import { baseUrl, fetchApi } from "../utils/fetchApi";

interface Props {
    properties: [PropertyType]
}

const Search = ({ properties }: Props) => {
  const [searchFilter, setSearchFilter] = useState(false);

  const router = useRouter();

  return (
    <Box>
      <Flex
        cursor="pointer"
        justifyContent="center"
        alignItems="center"
        p="2"
        backgroundColor="gray.100"
        border="1px"
        borderColor="gray.200"
        fontWeight="black"
        fontSize="lg"
        onClick={() => setSearchFilter((prevFilter) => !prevFilter)}
      >
        <Text>Search Property By Filter</Text>
        <Icon paddingLeft="2" w="7" as={BsFilter} />
      </Flex>

      {searchFilter && <SearchFilter />}

      <Text fontSize="2xl" p="4" fontWeight="bold">
        Properties{" "}
        {router.query.purpose === "for-sale"
          ? "For Sale"
          : router.query.purpose === "for-rent"
          ? "For Rent"
          : ""}
      </Text>

      {
        properties?.length <= 0 ?
        (
            <Flex justifyContent="center" alignItems="center" flexDirection="column">
                <Image src={Empty} alt="empty"/>
                <Text textAlign="center" fontSize="2xl" marginTop="2">No Result Found</Text>
            </Flex> 
        ) :
        (
            <Flex flexWrap="wrap">
                {properties.map(property => <Property key={property?.id} property={property}/>)}
            </Flex>
        )
      }
    </Box>
  );
};

export const getServerSideProps = async(query: queryType) => {
  const purpose = query.purpose || 'for-rent';
  const rentFrequency = query.rentFrequency || 'yearly';
  const minPrice = query.minPrice || '0';
  const maxPrice = query.maxPrice || '1000000';
  const roomsMin = query.roomsMin || '0';
  const bathsMin = query.bathsMin || '0';
  const sort = query.sort || 'price-desc';
  const areaMax = query.areaMax || '35000';
  const locationExternalIDs = query.locationExternalIDs || '5002';
  const categoryExternalID = query.categoryExternalID || '4';

  const data = await fetchApi(`${baseUrl}/properties/list?locationExternalIDs=${locationExternalIDs}&purpose=${purpose}&categoryExternalID=${categoryExternalID}&bathsMin=${bathsMin}&rentFrequency=${rentFrequency}&priceMin=${minPrice}&priceMax=${maxPrice}&roomsMin=${roomsMin}&sort=${sort}&areaMax=${areaMax}`);
    return {
        props: {
            properties: data?.hits
        }
    }
}

export default Search;
