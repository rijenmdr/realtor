import { Box, Flex } from "@chakra-ui/react";
import Banner from "../components/Banner/Banner";
import Header from "../components/Header/Header";
import Property from "../components/Property/Property";
import { PropertyType } from "../interface/property";

import {baseUrl, fetchApi} from '../utils/fetchApi';

interface Props {
  propertiesForSale: [PropertyType];
  propertiesForRent: [PropertyType];
}

const Home = ({propertiesForSale, propertiesForRent}: Props) => {

  console.log(propertiesForSale, propertiesForRent)

  return (
    <Box>

      <Header/>

      <Banner
        purpose="RENT A HOME"
        title1="Rental Homes for"
        title2="Everyone"
        desc1=" Explore from Apartments, builder floors, villas"
        desc2="and more"
        buttonText="Explore Renting"
        linkName="/search?purpose=for-rent"
        imageUrl="https://bayut-production.s3.eu-central-1.amazonaws.com/image/145426814/33973352624c48628e41f2ec460faba4"
      />

      <Flex flexWrap="wrap">
          {propertiesForRent.map(property => <Property property={property} key={property?.id} />)}
      </Flex>

      <Banner
        purpose="BUY A HOME"
        title1=" Find, Buy & Own Your"
        title2="Dream Home"
        desc1=" Explore from Apartments, land, builder floors,"
        desc2=" villas and more"
        buttonText="Explore Buying"
        linkName="/search?purpose=for-sale"
        imageUrl="https://bayut-production.s3.eu-central-1.amazonaws.com/image/110993385/6a070e8e1bae4f7d8c1429bc303d2008"
      />

      <Flex flexWrap="wrap">
        {propertiesForSale.map(property => <Property property={property} key={property?.id} />)}
      </Flex>
    </Box>
  );
};

export const getStaticProps = async() => {
  let propertiesForRent, propertiesForSale = [];
  try {
    propertiesForRent = await fetchApi(`${baseUrl}/properties/list?locationExternalIDs=5002&purpose=for-rent&hitsPerPage=8`);
    propertiesForSale = await fetchApi(`${baseUrl}/properties/list?locationExternalIDs=5002&purpose=for-sale&hitsPerPage=8`);
  } catch(err){
    console.log(err)
  }

  return {
    revalidate: true,
    props: {
      propertiesForSale: propertiesForSale?.hits,
      propertiesForRent: propertiesForRent?.hits
    },
  }
}

export default Home;
