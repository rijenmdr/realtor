import { Avatar, Box, Flex, Text } from "@chakra-ui/react";
import millify from "millify";
import { GetServerSideProps } from "next";
import React from "react";
import { BsGridFill } from "react-icons/bs";
import { FaBath, FaBed } from "react-icons/fa";
import { GoVerified } from "react-icons/go";
import ImageScrollbar from "../../components/ImageScrollbar/ImageScrollbar";
import { PropertyType } from "../../interface/property";
import { baseUrl, fetchApi } from "../../utils/fetchApi";

interface Props {
  property: PropertyType;
}

const PropertyDetail = ({
  property: {
    photos,
    isVerified,
    price,
    rentFrequency,
    agency,
    rooms,
    baths,
    area,
    title,
    description,
    type,
    purpose,
    furnishingStatus,
    amenities,
  },
}: Props) => {
  console.log(amenities);
  return (
    <Box maxWidth="1000px" m="auto" pt="6">
      {photos && <ImageScrollbar data={photos} />}

      <Box w="full" p="6">
        <Flex alignItems="center" justifyContent="space-between" paddingTop="2">
          <Flex alignItems="center">
            <Box paddingRight="3" color="green.400">
              {isVerified && <GoVerified />}
            </Box>
            <Text fontWeight="bold" fontSize="lg">
              Rs. {millify(price)}
              {rentFrequency && `/${rentFrequency}`}
            </Text>
          </Flex>
          <Box>
            <Avatar backgroundSize="cover" size="sm" src={agency?.logo?.url} />
          </Box>
        </Flex>

        <Flex
          alignItems="center"
          justifyContent="space-between"
          w="250px"
          color="blue.500"
          paddingTop="2"
        >
          {rooms} <FaBed /> | {baths} <FaBath /> | {millify(area)}sqft{" "}
          <BsGridFill />
        </Flex>

        <Box paddingTop="2">
          <Text fontSize="lg" fontWeight="bold">
            {title}
          </Text>
          <Text lineHeight="2" color="gray.600">
            <div dangerouslySetInnerHTML={{ __html: description }} />
          </Text>
        </Box>

        <Flex
          flexWrap="wrap"
          textTransform="uppercase"
          justifyContent="space-between"
        >
          <Flex
            w="400px"
            alignItems="center"
            justifyContent="space-between"
            borderBottom="1px"
            borderColor="gray.100"
            p="3"
          >
            <Text>Type</Text>
            <Text fontWeight="bold">{type}</Text>
          </Flex>
          <Flex
            w="400px"
            alignItems="center"
            justifyContent="space-between"
            borderBottom="1px"
            borderColor="gray.100"
            p="3"
          >
            <Text>Purpose</Text>
            <Text fontWeight="bold">{purpose}</Text>
          </Flex>
          {furnishingStatus && (
            <Flex
              justifyContent="space-between"
              w="400px"
              borderBottom="1px"
              borderColor="gray.100"
              p="3"
            >
              <Text>Furnishing Status</Text>
              <Text fontWeight="bold">{furnishingStatus}</Text>
            </Flex>
          )}
        </Flex>

        <Box>
          {amenities.length && (
            <Text fontSize="2xl" fontWeight="black" marginTop="5">
              Facilities:
            </Text>
          )}

          <Flex flexWrap="wrap">
            {amenities?.map((item) =>
              item?.amenities?.map((amenity) => (
                <Text
                  p="2"
                  bg="gray.200"
                  color="blue.400"
                  fontWeight="bold"
                  borderRadius="8"
                  m="2"
                  key={amenity?.text}
                >
                  {amenity?.text}
                </Text>
              ))
            )}
          </Flex>
        </Box>
      </Box>
    </Box>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const id = context?.params?.id;

  const data = await fetchApi(`${baseUrl}properties/detail?externalID=${id}`);

  console.log(data);

  return {
    props: {
      property: data,
    },
  };
};

export default PropertyDetail;
