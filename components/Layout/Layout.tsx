import { Box } from "@chakra-ui/react";
import { ReactNode } from "react";
import Footer from "../Footer/Footer";
import Navbar from "../Navbar/Navbar";

interface Props {
  children?: ReactNode;
}

const Layout = ({ children }: Props) => {
  return (
    <Box maxWidth="1280px" m="auto">
      <nav>
        <Navbar />
      </nav>
      <main style={{marginTop: "60px"}}>{children}</main>
      <footer>
        <Footer />
      </footer>
    </Box>
  );
};

export default Layout;
