import {
  Box,
  Flex,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spacer,
  Text,
} from "@chakra-ui/react";
import Link from "next/link";
import { BsSearch } from "react-icons/bs";
import { FaHome } from "react-icons/fa";

import { FcAbout, FcKey, FcMenu } from "react-icons/fc";

const Navbar = () => {
  return (
    <Flex
      p="2"
      borderBottom="1px"
      borderColor="gray.100"
      alignItems="center"
      position="fixed"
      top="0"
      bg="white"
      zIndex={10}
      right="0"
      left="0"
    >
      <Box fontSize="3xl" color="blue.400" fontWeight="bold">
        <Link href="/">Realtor</Link>
      </Box>

      <Spacer />

      <Box display={{ lg: "block", md: "none", base: "none" }}>
        <Flex alignItems="center">
          <Link href="/" passHref>
            <Text
              transition="all"
              transitionDuration=".4s"
              _hover={{ color: "blue.400" }}
              cursor="pointer"
              paddingRight="5"
            >
              Home
            </Text>
          </Link>
          <Link href="/search" passHref>
            <Text
              transition="all"
              transitionDuration=".4s"
              _hover={{ color: "blue.400" }}
              cursor="pointer"
              paddingRight="5"
            >
              Search
            </Text>
          </Link>
          <Link href="/search?purpose=for-sale" passHref>
            <Text
              transition="all"
              transitionDuration=".4s"
              _hover={{ color: "blue.400" }}
              cursor="pointer"
              paddingRight="5"
            >
              Buy Home
            </Text>
          </Link>
          <Link href="/search?purpose=for-rent" passHref>
            <Text
              transition="all"
              transitionDuration=".4s"
              _hover={{ color: "blue.400" }}
              cursor="pointer"
              paddingRight="5"
            >
              Rent Home
            </Text>
          </Link>
        </Flex>
      </Box>

      <Box display={{ xl: "none", lg: "none", md: "block" }}>
        <Menu>
          <MenuButton
            as={IconButton}
            icon={<FcMenu />}
            variant="outlined"
            color="red.400"
          />
          <MenuList>
            <Link href="/" passHref>
              <MenuItem icon={<FaHome />}>Home</MenuItem>
            </Link>
            <Link href="/search" passHref>
              <MenuItem icon={<BsSearch />}>Search</MenuItem>
            </Link>
            <Link href="/search?purpose=for-sale" passHref>
              <MenuItem icon={<FcAbout />}>Buy Home</MenuItem>
            </Link>
            <Link href="/search?purpose=for-rent" passHref>
              <MenuItem icon={<FcKey />}>Rent Home</MenuItem>
            </Link>
          </MenuList>
        </Menu>
      </Box>
    </Flex>
  );
};

export default Navbar;
