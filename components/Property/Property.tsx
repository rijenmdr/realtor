import { Avatar, Box, Flex, Text } from "@chakra-ui/react";
import millify from "millify";
import Image from "next/image";
import Link from "next/link";

import defaultImage from "../../public/images/defaultHouse.png";

import { GoVerified } from "react-icons/go";
import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { PropertyType } from "../../interface/property";

interface Props {
  property: PropertyType;
}

const Property = ({ property }: Props) => {
  return (
    <Link href={`/property/${property?.externalID}`} passHref>
      <Flex
        flexWrap="wrap"
        justifyContent="flex-start"
        p="5"
        paddingTop="0"
        w="420px"
        alignItems="center"
        cursor="pointer"
      >
        <Box>
          <Image
            src={
              property?.coverPhoto ? property?.coverPhoto?.url : defaultImage
            }
            alt="houses"
            width={400}
            height={260}
          />
        </Box>
        <Box w="full">
          <Flex
            alignItems="center"
            justifyContent="space-between"
            paddingTop="2"
          >
            <Flex alignItems="center">
              <Box paddingRight="3" color="green.400">
                {property?.isVerified && <GoVerified />}
              </Box>
              <Text fontWeight="bold" fontSize="lg">
                Rs. {millify(property?.price)}
                {property?.rentFrequency && `/${property?.rentFrequency}`}
              </Text>
            </Flex>
            <Box>
              <Avatar backgroundSize="cover" size="sm" src={property?.agency?.logo?.url} />
            </Box>
          </Flex>
        </Box>
        <Flex
          alignItems="center"
          justifyContent="space-between"
          w="250px"
          color="blue.500"
          paddingTop="2"
        >
          {property?.rooms} <FaBed /> | {property?.baths} <FaBath /> |{" "}
          {millify(property?.area)}sqft <BsGridFill />
        </Flex>
        <Text fontSize="lg" paddingTop="2">
          {property?.title?.length > 30
            ? `${property?.title?.substring(0, 30)}...`
            : property?.title}
        </Text>
      </Flex>
    </Link>
  );
};

export default Property;
