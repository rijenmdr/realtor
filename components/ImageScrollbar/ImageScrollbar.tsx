import { Box, Flex, Icon } from "@chakra-ui/react";
import Image from "next/image";
import { useContext } from "react"
import { ScrollMenu, VisibilityContext } from "react-horizontal-scrolling-menu"
import { FaArrowAltCircleLeft, FaArrowAltCircleRight } from "react-icons/fa";

interface Props {
    data: {
        id: string,
        url: string
    }[]
}

const LeftArrow = () => {
    const { scrollPrev } = useContext(VisibilityContext);
  
    return (
      <Flex justifyContent='center' alignItems='center' marginRight='1'>
        <Icon
          as={FaArrowAltCircleLeft}
          onClick={() => scrollPrev()}
          fontSize='2xl'
          cursor='pointer'
          d={['none','none','none','block']}
        />
      </Flex>
    );
  }
  
  const RightArrow = () => {
    const { scrollNext } = useContext(VisibilityContext);

    console.log("hello")
  
    return (
      <Flex justifyContent='center' alignItems='center' marginLeft='1'>
        <Icon
          as={FaArrowAltCircleRight}
          onClick={() => scrollNext()}
          fontSize='2xl'
          cursor='pointer'
          d={['none','none','none','block']}
      />
      </Flex>
    );
  }

const ImageScrollbar = ({data}: Props) => {
  return (
    <ScrollMenu LeftArrow={LeftArrow} RightArrow={RightArrow}> 
        {
            data?.map(image => (
                <Box key={image?.id} width="910px" itemID={image?.id} style={{overflow: "hidden"}}>
                    <Image
                        src={image?.url}
                        alt="images"
                        placeholder="blur"
                        blurDataURL={image?.url}
                        width={1000}
                        height={500}
                        sizes="(max-width: 500px) 100px, (max-width:1023px) 400px, 1000px"
                    />
                </Box>
            ))
        }
    </ScrollMenu>
  )
}

export default ImageScrollbar