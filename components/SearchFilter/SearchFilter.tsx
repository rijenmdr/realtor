import {
  Box,
  Button,
  Flex,
  Icon,
  Input,
  Select,
  Spinner,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { filterData, getFilterValues } from "../../utils/filterData";

import { MdCancel } from "react-icons/md";
import axios from "axios";
import { baseUrl, fetchApi } from "../../utils/fetchApi";
import { locationType } from "../../interface/location";

const SearchFilter = () => {
  const [filters, setFilters] = useState(filterData);
  const [showLocationFilter, setShowLocationFilter] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [loading, setLoading] = useState<boolean>(false);
  const [locationData, setLocationData] = useState([]);
  const [showSuggestion, setShowSuggestion] = useState(false);

  const router = useRouter();

  const handleLocationChange = (value: string) => {
    setSearchTerm(value);
    setShowSuggestion(true);
  };

  const changeFilterProperty = (filterValues: object) => {
    const path = router.pathname;
    const query = router.query;

    const values = getFilterValues(filterValues);
    values.forEach((item) => {
      if (item?.value) {
        query[item.name] = item?.value;
      }
    });

    router.push({ pathname: path, query });
  };

  useEffect(() => {
    if (searchTerm !== "") {
      const fetchData = async () => {
        setLoading(true);
        const data = await fetchApi(
          `${baseUrl}auto-complete?query=${searchTerm}`
        );
        setLoading(false);
        setLocationData(data?.hits);
      };
      fetchData();
    }
  }, [searchTerm]);

  const selectSuggestion = (location: locationType) => {
    changeFilterProperty({ locationExternalIDs: location?.externalID });
    setShowSuggestion(false);
    setSearchTerm(location?.name);
  };

  return (
    <Flex alignItems="center" flexWrap="wrap" bg="gray.100" p="4">
      {filters.map((filter) => (
        <Box key={filter?.queryName}>
          <Select
            placeholder={filter?.placeholder}
            w="fit-content"
            p="2"
            onChange={(e) =>
              changeFilterProperty({ [filter?.queryName]: e.target.value })
            }
          >
            {filter?.items.map((item) => (
              <option key={item?.value} value={item?.value}>
                {item?.name}
              </option>
            ))}
          </Select>
        </Box>
      ))}
      <Flex flexDir="column">
        <Button
          onClick={() => setShowLocationFilter((prev) => !prev)}
          border="1px"
          borderColor="gray.200"
          mt="2"
        >
          Search Location
        </Button>
        {showLocationFilter && (
          <Flex flexDir="column" pos="relative" pt="2">
            <Input
              placeholder="Enter Location"
              value={searchTerm}
              onChange={(e) => handleLocationChange(e.target.value)}
              w="300px"
              focusBorderColor="gray.300"
            />
            {searchTerm !== "" && (
              <Icon
                as={MdCancel}
                pos="absolute"
                cursor="pointer"
                right="5"
                top="5"
                zIndex="100"
                onClick={() => setSearchTerm("")}
              />
            )}
          </Flex>
        )}
        {loading && <Spinner margin="auto" mt="5" />}

        {locationData && locationData.length !== 0 && showSuggestion && (
          <Box height="300px" overflow="auto">
            {locationData?.map((location: locationType) => (
              <Box
                onClick={() => selectSuggestion(location)}
                key={location?.id}
              >
                <Text
                  cursor="pointer"
                  bg="gray.200"
                  p="2"
                  borderBottom="1px"
                  borderColor="gray.100"
                >
                  {location?.name}
                </Text>
              </Box>
            ))}
          </Box>
        )}
      </Flex>
    </Flex>
  );
};

export default SearchFilter;
