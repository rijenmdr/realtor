import { Box, Button, Flex, Text } from "@chakra-ui/react"
import Link from "next/link"

const Header = () => {
  return (
    <Flex
        width="100%"
        height="100vh"
        backgroundSize="cover"
        backgroundAttachment="fixed"
        backgroundPosition= "center"
        backgroundRepeat= "no-repeat"
        alignItems="center"
        justifyContent="center"
        p="9"
        backgroundImage= "linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url('https://www.slidebackground.com/uploads/real-estate-background/real-estate-homebuy-home-banner-background-homebuym-1.jpg')" 
    >
        <Flex flexDir="column">
            <Text color="white" fontSize="5xl" fontWeight="bold" textAlign="center">Realtor</Text>
            <Text color="white" maxW="400px" textAlign="center" fontSize="xl" fontWeight="bold">Find the house of your dream that really suites you.</Text>

            <Link href="/search" passHref>
                <Button  mt="4">Explore</Button>
            </Link>
        </Flex>
    </Flex>
  )
}

export default Header